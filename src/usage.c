/*
 * author: Darren Bounds <dbounds@intrusense.com>
 * copyright: Copyright (C) 2002 by Darren Bounds
 * license: This software is under GPL version 2 of license
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * 4g8 official page at http://.net
 */

#include "usage.h"

void
print_usage()
{
    fprintf(stdout, "Usage: 4g8 -i <device> -g <gw_ip> -G <gw_mac> -s <host_ip> -S <host_mac> [-w file] [-Xh]\n\n");

    fprintf(stdout, "Required:\n");
    fprintf(stdout, "  -g gw_ip    IP address of gateway to impersonate\n");
    fprintf(stdout, "  -G gw_mac   MAC address of gateway to impersonate\n");
    fprintf(stdout, "  -s host_ip  IP address of victim host\n");
    fprintf(stdout, "  -S host_mac MAC address of victim host\n");
    fprintf(stdout, "\n");

    fprintf(stdout, "Options:\n");    
    fprintf(stdout, "  -h          Display this message\n");
    fprintf(stdout, "  -i device   Device to listen on\n");
    fprintf(stdout, "  -w file     Write captured data to file\n");
    fprintf(stdout, "  -X          Dump the packet in hex and ascii\n");
    fprintf(stdout, "\n");
    
    fprintf(stdout, "Version: %s\n", FG_VERSION); 
    fprintf(stdout, "Author:  %s\n", FG_AUTHOR);
    fprintf(stdout, "Website: %s\n", FG_SITE);
    fprintf(stdout, "\n");
   
    exit(SUCCESS);
}

void
print_version()
{
    fprintf(stdout, "Version: %s\n", FG_VERSION);
    fprintf(stdout, "Author:  %s\n", FG_AUTHOR);
    fprintf(stdout, "Website: %s\n", FG_SITE);
    fprintf(stdout, "\n");

    exit(SUCCESS);
}

